const http = require("http");

const port = 3000;


const server = http.createServer((req, res)=>{
	// http method of the incoming request can be accessed via req.method
		// GET method - retrieving/reading information; default method
	if (req.url ==="/items" && req.method ==="GET"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Data retrieved from database");
	}


	/*
		create a "items" url with POST method
		the response should be "Data to be sent to the database" with 200 as s
		status code and plain text as the content type

	*/
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);