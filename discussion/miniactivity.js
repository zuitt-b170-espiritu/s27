const http = require("http");

const port = 4000;


const server = http.createServer((req, res)=>{
	// http method of the incoming request can be accessed via req.method
		// GET method - retrieving/reading information; default method
	if (req.url ==="/items" && req.method ==="POST"){
		res.writeHead(200,{"Content-Type":"text/plain"});
		res.end("Data retrieved from database");
	}


	/*
		create a "items" url with POST method
		the response should be "Data to be sent to the database" with 200 as s
		status code and plain text as the content type

	*/
	/*
		POST, PUT and DELETE operation do not work in th browser unlike the GET method.
		with this, Postman solves the problem by simulating a frontend for the developers to test their codes

	*/
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);